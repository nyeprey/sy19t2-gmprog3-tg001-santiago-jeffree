﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class LoadScenes : MonoBehaviour
{
	public void LoadGame()
	{
		SceneManager.LoadScene("3.Field");
	}

	public void LoadAnimations()
	{
		SceneManager.LoadScene("Animations Scene");
	}

	public void LoadMainMenu()
	{
		SceneManager.LoadScene("MainMenu");
	}
    public void LoadVillage()
    {
        SceneManager.LoadScene("Village");
    }
}
