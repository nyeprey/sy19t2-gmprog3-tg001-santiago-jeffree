﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour
{
    public GameObject[] spawnees;
    public Transform[] spawnPoints;
    private bool stopSpawn = false;
    public int spawnNum;
    public int maxSpawn;
	private float spawnTimer = 0;
    int randomPosInt;
    int randomInt;

	public GameObject itemSpawner;

    void Start()
    {
       
    }

	void Update()
	{
		spawnTimer += Time.deltaTime;

		if(spawnTimer >= 1f && GameObject.FindGameObjectsWithTag("enemyTag").Length < maxSpawn)
		{
			SpawnRandom();
			spawnTimer = 0f;
		}
	}

    void SpawnRandom()
    {
        randomPosInt = Random.Range(0, spawnPoints.Length);
        randomInt = Random.Range(0, spawnees.Length);
		GameObject reference = Instantiate(spawnees[randomInt], spawnPoints[randomPosInt].transform.position, Quaternion.identity) as GameObject;
		reference.GetComponent<EnemyBehavior>().itemSpawner = itemSpawner;

        // if you have less enemy base on maxspawned
        // continue spawning monster
       // else
       //dont spawn then but continue the loop
    }
}





















