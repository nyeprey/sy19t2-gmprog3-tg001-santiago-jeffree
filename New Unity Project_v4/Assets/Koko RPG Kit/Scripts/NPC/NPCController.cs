﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    private bool isTalking = false;
    private Animator npcAnimation;

    private void Start()
    {
        npcAnimation = gameObject.GetComponent<Animator>();
    }

    public void Talk(Transform actor)
    {
        transform.LookAt(actor);
        npcAnimation.Play("AnimateTalk");
    }
    public void Exit()
    {
        npcAnimation.Play("AnimateIdle");
    }
}
