﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KokoAnimationController : MonoBehaviour {

    #region Attributes

    private Animator animator;

    private const string IDLE_ANIMATION_BOOL = "idle";
    private const string DRAWBLADE_ANIMATION_BOOL = "drawBlade";
    private const string PUTBLADE_ANIMATION_BOOL = "putBlade";
    private const string RUN_ANIMATION_BOOL = "run";
    private const string DEAD_ANIMATION_BOOL = "dead";
    private const string RUNNO_ANIMATION_BOOL = "runNo";
    private const string DAMAGE_ANIMATION_BOOL = "damage";
    private const string ATTACK_ANIMATION_BOOL = "attack";
    private const string COMBO_ANIMATION_BOOL = "combo";
    private const string ATTACKSTANDY_ANIMATION_BOOL = "attackStandy";
    private const string SKILL_ANIMATION_BOOL = "skill";

    #endregion

    #region Animate Functions

    public void AnimateIdle()
    {
        Animate(IDLE_ANIMATION_BOOL);
    }

    public void AnimateDrawBlade()
    {
        Animate(DRAWBLADE_ANIMATION_BOOL);
    }
    public void AnimatePutBlade()
    {
        Animate(PUTBLADE_ANIMATION_BOOL);
    }
    public void AnimateRun()
    {
        Animate(RUN_ANIMATION_BOOL);
    }
    public void AnimateDead()
    {
        Animate(DEAD_ANIMATION_BOOL);
    }
    public void AnimateRunNo()
    {
        Animate(RUNNO_ANIMATION_BOOL);
    }
    public void AnimateDamage()
    {
        Animate(DAMAGE_ANIMATION_BOOL);
    }
    public void AnimateAttack()
    {
        Animate(ATTACK_ANIMATION_BOOL);
    }
    public void AnimateCombo()
    {
        Animate(COMBO_ANIMATION_BOOL);
    }
    public void AnimateAttackStandy()
    {
        Animate(ATTACKSTANDY_ANIMATION_BOOL);
    }
    public void AnimateSkill()
    {
        Animate(SKILL_ANIMATION_BOOL);
    }
    #endregion

    private void Start()
    {
            animator = GetComponent<Animator>();   
    }

    private void Animate(string boolName)
    {
        DisableOtherAnimations(animator, boolName);

        animator.SetBool(boolName, true);
    }

    private void DisableOtherAnimations(Animator animator, string animation)
    {
        foreach(AnimatorControllerParameter parameter in animator.parameters)
        {
            if(parameter.name != animation)
            {
                animator.SetBool(parameter.name, false);
            }
        }
    }
}
