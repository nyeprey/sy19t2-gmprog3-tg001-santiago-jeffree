﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCSript : MonoBehaviour {

    #region Attributes

    private Animator animator;

    private const string NPC_IDLE_ANIMATE_BOOL = "idle";
    private const string NPC_TALK_ANIMATE_BOOL = "talk";

    #endregion

    #region

    public void NPCAnimateIdle()
    {
        Animate(NPC_IDLE_ANIMATE_BOOL);
    }
    public void NPCAnimateTalk()
    {
        Animate(NPC_TALK_ANIMATE_BOOL);
    }
    #endregion
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
	}

    private void Animate(string boolName)
    {
        DisableOtherAnimations(animator, boolName);

        animator.SetBool(boolName, true);
    }
    private void DisableOtherAnimations(Animator animator, string animation)
    {
        foreach(AnimatorControllerParameter parameter in animator.parameters)
        {
            if(parameter.name != animation)
            {
                animator.SetBool(parameter.name, false);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
