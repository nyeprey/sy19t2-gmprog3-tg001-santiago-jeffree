﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationScript : MonoBehaviour {

    #region Attributes

    private Animator animator;

    private const string E_IDLE_ANIMATION_BOOL = "idle";
    private const string E_WALK_ANIMATION_BOOL = "walk";
    private const string E_RUN_ANIMATION_BOOL = "run";
    private const string E_ATTACK_ANIMATION_BOOL = "attack";
    private const string E_ATTACK01_ANIMATION_BOOL = "attack01";
    private const string E_DAMAGE_ANIMATION_BOOL = "damage";
    private const string E_DEAD_ANIMATION_BOOL = "dead";

    #endregion

    #region

    public void EAnimateIdle()
    {
        AnimateE(E_IDLE_ANIMATION_BOOL);
    }
    public void EAnimateWalk()
    {
        AnimateE(E_WALK_ANIMATION_BOOL);
    }
    public void EAnimateRun()
    {
        AnimateE(E_RUN_ANIMATION_BOOL);
    }
    public void EAnimateAttack()
    {
        AnimateE(E_ATTACK_ANIMATION_BOOL);
    }
    public void EAnimateAttack01()
    {
        AnimateE(E_ATTACK01_ANIMATION_BOOL);
    }
    public void EAnimateDamage()
    {
        AnimateE(E_DAMAGE_ANIMATION_BOOL);
    }
    public void EAnimateDead()
    {
        AnimateE(E_DEAD_ANIMATION_BOOL);
    }

    #endregion

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
	}
	
    private void AnimateE(string boolName)
    {
        DisableOtherAnimations(animator, boolName);

        animator.SetBool(boolName, true);
    }

    private void DisableOtherAnimations(Animator animator, string animation)
    {
        foreach(AnimatorControllerParameter parameter in animator.parameters)
        {
            if(parameter.name != animation)
            {
                animator.SetBool(parameter.name, false);
            }
        }
    }
}
