﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterChooseScript : MonoBehaviour
{
	public float cameraSpeed;

	public GameObject playerPos;
	public GameObject enemyPos;
	public GameObject NPCPos;

	private GameObject targetPos;

	public void MoveToPlayer()
	{
		targetPos = playerPos;
	}

	public void MoveToEnemy()
	{
		targetPos = enemyPos;
	}

	public void MoveToNPC()
	{
		targetPos = NPCPos;
	}

	void Start()
	{
		targetPos = playerPos;
	}

	void Update()
	{
		if(gameObject.transform.position != targetPos.transform.position)
		{
			gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, targetPos.transform.position, cameraSpeed * Time.deltaTime);
		}
	}
}
