﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : Item2D
{
    public int value = 10;
    public Gold()
    {
        itemName = "Gold";
        description = "adds gold";

    }
    public override void OnUse(GameObject player)
    {
        player.GetComponent<StatSheet>().CollectGold(this.gameObject.GetComponent<StatSheet>().GetGold() + value);
    }
}
