﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPotion : Item2D
{
    public HealthPotion()
    {
        itemName = "Health Potion";
        description = "heals 50 hp";
        gold = 10;
    }

    public override void OnUse(GameObject player)
    {
        player.GetComponent<StatSheet>().Heal(50);
    }
}
