﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSpawner : MonoBehaviour
{
	public List<GameObject> item3DPrefabs = new List<GameObject>();
    public GameObject gold;

	public Sprite healthPotionImage;

	void Start()
	{
        gold.GetComponent<Item3D>().item2D.GetComponent<Item2D>();
		item3DPrefabs[0].GetComponent<Item3D>().item2D.GetComponent<Item2D>().itemImage = healthPotionImage;
	}

	public void SpawnRandomObject(Vector3 spawnLocation)
	{
		//choose random prefab, instantiate as gameobject, change game object's location to spawnlocation
		GameObject reference = Instantiate(item3DPrefabs[Random.Range(0, item3DPrefabs.Count)]) as GameObject;
		reference.transform.position = spawnLocation;
	}
    public void SpawnGold(Vector3 spawnLocation)
    {
        GameObject reference = Instantiate(gold) as GameObject;
        reference.transform.position = spawnLocation;
    }
}
