﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour {
	public int gold;
	public List<GameObject> contents = new List<GameObject>();

	public GameObject inventoryWindow;
	public Sprite defaultButtonImage;

	private InventoryButton[] buttons;

	void Start()
	{
		buttons = inventoryWindow.GetComponentsInChildren<InventoryButton>();
	}

	void Update()
	{
		for(int i = 0; i < buttons.Length; i++)
		{
			buttons[i].item2D = null;
			buttons[i].GetComponent<Image>().sprite = null;

			if(i < contents.Count)
			{
				buttons[i].item2D = contents[i];
				buttons[i].GetComponent<Image>().sprite = contents[i].GetComponent<Item2D>().itemImage;
			}
			else
			{
				break;
			}
		}

        if(Input.GetKey(KeyCode.Tab))
        {
            inventoryWindow.SetActive(true);
        }
        if(Input.GetKey(KeyCode.Escape))
        {
            inventoryWindow.SetActive(false);
        }
        
	}
}
