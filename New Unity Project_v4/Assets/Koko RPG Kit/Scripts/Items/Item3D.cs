﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item3D : MonoBehaviour {
	public GameObject item2D;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			other.gameObject.GetComponent<PlayerInventory>().contents.Add(item2D);

			if(item2D.GetComponent<Item2D>().itemName == "Gold")
			{
				item2D.GetComponent<Item2D>().OnUse(other.gameObject);
			}

			Destroy(gameObject);
		}
	}
}
