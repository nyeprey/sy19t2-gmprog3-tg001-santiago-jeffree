﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryButton : MonoBehaviour {
	public GameObject player;

	public GameObject item2D;

	public void UseItem()
	{
		if(item2D != null)
		{
			item2D.GetComponent<Item2D>().OnUse(player);
			player.GetComponent<PlayerInventory>().contents.Remove(item2D);
			item2D = null;
		}
	}
}
