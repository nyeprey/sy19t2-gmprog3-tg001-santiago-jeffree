﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item2D : MonoBehaviour {
	public string itemName;
	public Sprite itemImage;
	public string description;
	public int gold;

	public virtual void OnUse(GameObject player)
	{

	}
}
