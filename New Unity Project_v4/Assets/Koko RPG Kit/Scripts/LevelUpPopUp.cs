﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpPopUp : MonoBehaviour {
	private float timer;

	void OnEnable()
	{
		timer = 1.5f;
		gameObject.GetComponent<Text>().fontSize = 1;
	}

	void Update()
	{
		if(timer <= 0.0f)
		{
			gameObject.SetActive(false);
		}

		timer -= Time.deltaTime;
		gameObject.GetComponent<Text>().fontSize += 3;
	}
}
