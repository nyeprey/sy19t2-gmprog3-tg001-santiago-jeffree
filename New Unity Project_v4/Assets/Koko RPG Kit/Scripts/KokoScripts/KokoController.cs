﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class KokoController : MonoBehaviour
{
    [SerializeField]
    private Camera cam;
    public GameObject pointObject;
    public GameObject inventory;
	public NavMeshAgent agent;
	public RaycastHit hit;
	private GameObject pointHandler;
	private GameObject target;
	Collider enemyObjectCollider;
	public bool isMoving = false;
    private bool enemyClicked = false;
    private int curStateIndex = 0;
    // 0 = idle, 1 = run, 2 = 

    private void NonAttackState()
    {
        gameObject.GetComponent<KokoAnimationController>().AnimateIdle();
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            isMoving = true;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (pointHandler == null)
                {
                    pointHandler = Instantiate(pointObject) as GameObject;
                }
                pointHandler.transform.position = hit.point;
                agent.SetDestination(hit.point);
            }
            // check if enemy hit
            if ( hit.collider.tag == "enemyTag")
            {
                curStateIndex = 1;
				target = hit.collider.gameObject;
            }
        }
        // check distance between koko and pointer
        if (Vector3.Distance(this.gameObject.transform.position, hit.point) < 1.0f)
        {
            Destroy(pointHandler);
            isMoving = false;
        }

        if (isMoving == true)
            gameObject.GetComponent<KokoAnimationController>().AnimateRun();
        else if (isMoving == false)
            gameObject.GetComponent<KokoAnimationController>().AnimateIdle();
    }
    private void AttackState()
    {
		if(target == null)
		{
			curStateIndex = 0;
			return;
		}

        Destroy(pointHandler);
        if(Vector3.Distance(this.gameObject.transform.position, hit.collider.gameObject.transform.position) < 1.0f)
        gameObject.GetComponent<KokoAnimationController>().AnimateAttack();
        if (Input.GetMouseButtonDown(0))
        {
            isMoving = true;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (pointHandler == null)
                {
                    pointHandler = Instantiate(pointObject) as GameObject;
                }
                pointHandler.transform.position = hit.point;
                agent.SetDestination(hit.point);
            }
            curStateIndex = 0;
        }
    }
	private void DeathState()
	{
		gameObject.GetComponent<KokoAnimationController>().AnimateDead();
	}
    private void Start()
    {
        // fix
        enemyObjectCollider = GetComponent<Collider>();
        enemyObjectCollider.isTrigger = false;
    }
    void FixedUpdate()
    {
		if(gameObject.GetComponent<StatSheet>().GetCurrentHealth() <= 0)
		{
			curStateIndex = 2;
		}

        if (curStateIndex == 0)
            NonAttackState();
        else if (curStateIndex == 1)
            AttackState();
		else if (curStateIndex == 2)
			DeathState();

        if (Input.GetKey(KeyCode.Q))
        {
            inventory.SetActive(true);
        }
    }

	void DamageTo()
	{
		target.GetComponent<StatSheet>().Damage(this.gameObject.GetComponent<StatSheet>().GetAttackPower());
		// enemy.Damage(player.attackPower);
	}
}

