﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopDetection : MonoBehaviour
{
    public GameObject thisCharacter;
    public GameObject storePrompt;
    public GameObject store;
    private bool isInStore = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Store")
        {
            if (storePrompt != null)
            {
                storePrompt.SetActive(true);
                isInStore = true;
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Store")
        {
            if (storePrompt != null)
            {
                storePrompt.SetActive(false);
                isInStore = false;
            }
        }
    }
    private void Update()
    {
        if (isInStore == true && Input.GetKeyDown("space"))
        {
            store.SetActive(true);
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            store.SetActive(false);
            if(Time.timeScale == 0)
            {
                Time.timeScale = 1;
            }
        }
    }
}
