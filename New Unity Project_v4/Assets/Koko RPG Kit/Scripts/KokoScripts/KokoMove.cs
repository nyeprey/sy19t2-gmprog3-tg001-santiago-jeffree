﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KokoMove : MonoBehaviour {

    public GameObject pointObject;
    private GameObject pointHandler;
    private Vector3 mousePos;
    Vector3 targPosition;
    Vector3 lookAtTarget;
    Quaternion playerRotate;
    float rotSpeed = 3;
    float moveSpeed = 9;
    bool isMoving = false;
    public Transform target;
    UnityEngine.AI.NavMeshAgent agent;
    // Use this for initialization
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            SetTargetPosition();

            if (isMoving)
            {
                Ray rayCast = Camera.main.ScreenPointToRay(Input.mousePosition);
                pointHandler = Instantiate(pointObject, rayCast.GetPoint(15), Quaternion.identity) as GameObject;
                agent.SetDestination(target.position);
            }
            else
            {

            }
        }
        if (isMoving)
        {
            Move();
        }
    }

    void SetTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            targPosition = hit.point;
            this.transform.LookAt(targPosition);
            lookAtTarget = new Vector3(targPosition.x - transform.position.x,
                transform.position.y,
                targPosition.z - transform.position.z);
            isMoving = true;

            //if (isMoving)
            //{
            //    GameObject reference = Instantiate(pointObject);
            //    reference.transform.position = Input.mousePosition;
            //}
            //else
            //{

            //}
        }
    }
    void Move()
    {
        //move koko from position towards targPosition
        transform.position = Vector3.MoveTowards(transform.position,
            targPosition, moveSpeed * Time.deltaTime);

        //stop koko when targPosition reached
        if (Vector3.Distance(transform.position, targPosition) <= 1.0f)
        isMoving = false;
    }
}