﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehavior : MonoBehaviour
{

    public float lookRadius = 2f;
    public float attackRadius = 0;
    private Vector3 randomPos;
	GameObject target;
    NavMeshAgent agent;
    private int curStateIndex = 0;
	private float deathTime = 3.0f;
   // public float distance = 10f;
    float waitTime = 5.0f;

	public GameObject itemSpawner;

	Vector3 targetPosition;
    // 0 = idle, 1 = patrol, 2 = chase, 3 = attack, 4 = death
	public void ChangeState(int index)
	{
		curStateIndex = index;
	}
    // !!! fix patrol state and transition from idle to patrol state !!!
    private void IdleState() // curStateIndex == 0
    {
        gameObject.GetComponent<EnemyAnimationScript>().EAnimateIdle();
        float distance = Vector3.Distance(target.transform.position, this.gameObject.transform.position);
        if (waitTime <= 0)
        {
            curStateIndex = 1;

			float minX = -10.0f;
			float maxX = 10.0f;
			float minZ = -10.0f;
			float maxZ = 10.0f;
			//float speed = 2.0f;

			float xPos = Random.Range(minX, maxX);
			float zPos = Random.Range(minZ, maxZ);

			targetPosition = new Vector3(xPos, 0, zPos) + this.gameObject.transform.position;
			agent.SetDestination(targetPosition);
        }
        else
        { waitTime -= Time.deltaTime; }

        // if player is within lookradius, switch to chase state
        if (distance <= lookRadius)
        {
            curStateIndex = 2;
        } 

    }
    private void PatrolState() // curStateIndex == 1
    {
		gameObject.GetComponent<EnemyAnimationScript>().EAnimateWalk();
		if(Vector3.Distance(this.gameObject.transform.position, targetPosition) <= 5.0f)
		{
			curStateIndex = 0;
			waitTime = 5.0f;
		}
		//(Vector3.Distance(curEnemyPos, targetPosition) <= 5.0f)
       // transform.Translate(Vector3.MoveTowards());
    }
    private void ChaseState()
	{
        gameObject.GetComponent<EnemyAnimationScript>().EAnimateRun();
        // start chasing
		agent.SetDestination(target.transform.position);
        Debug.Log("enemy moving");

		float distance = Vector3.Distance(target.transform.position, this.gameObject.transform.position);
        // if player is within attackRadius, switch to attackState
        if (distance <= attackRadius)
        {
            curStateIndex = 3;
        }
         //if(agent.remainingDistance)
    }
    private void AttackState()
	{
        gameObject.GetComponent<EnemyAnimationScript>().EAnimateAttack();
		float distance = Vector3.Distance(target.transform.position, transform.position);
        // stop moving
        // start attacking

        if(distance >= attackRadius)
        {
            curStateIndex = 2;
        }
    }
    private void DeathState()
	{
		gameObject.GetComponent<EnemyAnimationScript>().EAnimateDead();
		agent.SetDestination(this.transform.position);

		if(deathTime > 0)
		{
			deathTime -= Time.deltaTime;
		}
		else if(deathTime <= 0)
		{
			Debug.Log(gameObject.GetComponent<StatSheet>().GetExpEarn());
			target.GetComponent<StatSheet>().EXPGain(this.gameObject.GetComponent<StatSheet>().GetExpEarn());

            if (itemSpawner != null)
			{
				itemSpawner.GetComponent<ItemSpawner>().SpawnRandomObject(this.transform.position);
                itemSpawner.GetComponent<ItemSpawner>().SpawnGold(this.transform.position);
            }
			Destroy(gameObject);

		}
	}
    void Start()
    {
        target = PlayerManager.instance.player;
		targetPosition = this.gameObject.transform.position;
        agent = GetComponent<NavMeshAgent>();
		float distance = Vector3.Distance(target.transform.position, this.gameObject.transform.position);
    }
    void Update()
    {
		if(gameObject.GetComponent<StatSheet>().GetCurrentHealth() <= 0)
			curStateIndex = 4;
		
        if(curStateIndex == 0)
        {
            IdleState();
        }
        else if(curStateIndex == 1)
        {
            PatrolState();
        }
        else if(curStateIndex == 2)
        {
            ChaseState();
        }
        else if(curStateIndex == 3)
        {
            AttackState();
        }
		else if(curStateIndex == 4)
		{
			DeathState();
		}
    }
	void DamageTo()
	{
		target.GetComponent<StatSheet>().Damage(this.gameObject.GetComponent<StatSheet>().GetAttackPower());
        Debug.Log("damaging player");
		// enemy.Damage(player.attackPower);
	}
   }
