﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetector : MonoBehaviour
{
	public GameObject thisCharacter;

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			thisCharacter.GetComponent<EnemyBehavior>().ChangeState(2);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag == "Player")
		{
			thisCharacter.GetComponent<EnemyBehavior>().ChangeState(0);
		}
	}
}
