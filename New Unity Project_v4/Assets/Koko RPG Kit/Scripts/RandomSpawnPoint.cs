﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnPoint : MonoBehaviour {

    public GameObject spawnPoint;

    public float minX = 0;
    public float maxX = 10;
    public float minY = 0;
    public float maxY = 10;
    public float minZ = 0;
    public float maxZ = 10;
	
	
    void SpawnPoint()
    {
        float x = Random.Range(minX, maxX);
        float y = Random.Range(minY, maxY);
        float z = Random.Range(minZ, maxZ);
    }
}
