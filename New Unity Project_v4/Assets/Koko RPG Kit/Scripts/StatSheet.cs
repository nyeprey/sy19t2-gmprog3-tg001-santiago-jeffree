﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatSheet : MonoBehaviour
{
	public Image healthBar;
	public Image EXPBar;
	public Text textDisplay;
	public GameObject levelUpPrompt;
	public GameObject strButton;
	public GameObject vitButton;

	public float vit;
	public float str;
	public float expEarn;

	public float curHealth; // dead at 0, never exceeds maxHealth
	private float maxHealth;
	private float atkPwr;
	private float exp = 0f;
	private float expToLvlUp = 10f;
	private int level = 1;
	private int statPoints = 0;
    private int gold = 0;

	public float GetCurrentHealth()
	{
		if (curHealth <= 0)
		{
			curHealth = 0;
		}
		return curHealth;
	}

	public float GetAttackPower()
	{
		return atkPwr;
	}

	public float GetMaxHealth()
	{
		return maxHealth;
	}

	public float GetExpEarn()
	{
		return expEarn;
	}
    public int GetGold()
    {
        return gold;
    }

	void Start()
	{
		CalculateStats();
		curHealth = maxHealth;
	}

	void Update()
	{
		if(healthBar != null)
			healthBar.fillAmount = curHealth / maxHealth;
		if(EXPBar != null)
			EXPBar.fillAmount = exp / expToLvlUp;
		if(exp >= expToLvlUp)
		{
			if(levelUpPrompt != null)
			{
				levelUpPrompt.SetActive(true);
			}
			level ++;
			exp = 0;
			CalculateStats();
			curHealth = maxHealth;
			statPoints += 2;
		}
		if(textDisplay != null)
		{
			textDisplay.text = "LVL: " + level + "\n" +
				"STR: " + str + "\n" +
				"VIT: " + vit + "\n" +
                "GOLD: " + gold;
		}

		if(statPoints <= 0 && vitButton != null && strButton != null)
		{
			vitButton.SetActive(false);
			strButton.SetActive(false);
		}
		else if(statPoints > 0 && vitButton != null && strButton != null)
		{
			vitButton.SetActive(true);
			strButton.SetActive(true);
		}
			
	}

	public void CalculateStats()
	{
		maxHealth = vit * 10f;
		atkPwr = str * 2f;
	}

	public void EXPGain(float value)
	{
		exp += value;
		Debug.Log("gaining");
	}

	public void increseSTR()
	{
		statPoints -= 1;
		str += 1;
		CalculateStats();
	}

	public void increaseVIT()
	{
		statPoints -= 1;
		vit += 1;
		CalculateStats();
		curHealth = maxHealth;
	}

	public void Damage(float value)
	{
		curHealth -= value;
	}

	public void Heal(float value)
	{
		curHealth += value;
	}
    public void CollectGold(int value)
    {
        gold += value;
    }
}
